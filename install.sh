#!/bin/bash
echo "Begin install"
brew cleanup
node --version
brew install watchman
brew tap AdoptOpenJDK/openjdk
brew cask install adoptopenjdk8
npm install -g react-native-cli
npm install --force
sudo gem install cocoapods
sudo gem install fastlane -NV
pod --version
echo "react-native version"
react-native info
xcode-select -v
xcodebuild -version
cd ios
pod install && cd ..
